package com.HomemadeFoodOrder.entity;

import java.sql.Blob;
import java.sql.Date;
import java.sql.Time;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "food_list_tbl")
public class FoodlistEntity 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="food_id", unique=true)
	private int id;
	
	@Lob
	@Column(name = "food_image")
	private Blob image;
	
	@Column(name = "food_name")
	private String name;
	
	@Column(name = "food_description")
	private String descr;
	
	@Column(name = "food_price")
	private int price;
	
	@Column(name = "food_category")
	private String category;
	
	@Column(name = "qnt")
	private int  qnt;
	
	@Column(name = "unit")
	private Time unit;
	
	@Column(name = "valid_till_date")
	private Date date;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "homemaker_id")
	private HomemakerEntity homemaker;
	
	public FoodlistEntity() {}

	public FoodlistEntity(Blob image, String name, String descr, int price, 
							String category, int qnt, Time unit, Date date) 
	{
		super();
		this.image = image;
		this.name = name;
		this.descr = descr;
		this.price = price;
		this.category = category;
		this.qnt = qnt;
		this.unit = unit;
		this.date = date;
	}

	public int getId() 
	{
		return id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	public Blob getImage() 
	{
		return image;
	}

	public void setImage(Blob image) 
	{
		this.image = image;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getDescr() 
	{
		return descr;
	}

	public void setDescr(String desc) 
	{
		this.descr = desc;
	}

	public int getPrice() 
	{
		return price;
	}

	public void setPrice(int price) 
	{
		this.price = price;
	}

	public String getCategory() 
	{
		return category;
	}

	public void setCategory(String category) 
	{
		this.category = category;
	}

	public int getQnt() 
	{
		return qnt;
	}

	public void setQnt(int qnt) 
	{
		this.qnt = qnt;
	}

	public Time getUnit() 
	{
		return unit;
	}

	public void setUnit(Time unit) 
	{
		this.unit = unit;
	}

	public Date getDate() 
	{
		return date;
	}

	public void setDate(Date date) 
	{
		this.date = date;
	}
	
	public HomemakerEntity getHomemaker() 
	{
		return homemaker;
	}

	public void setHomemaker(HomemakerEntity homemaker) 
	{
		this.homemaker = homemaker;
	}

}
