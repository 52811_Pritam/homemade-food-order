import "./App.css"
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link,
    useNavigate,
} from "react-router-dom"
import { useEffect, useState } from "react"

import axios from "axios"
// https://axios-http.com/docs/intro

import Header from "./components/Header"
import Footer from "./components/Footer"
import Scroll from "./components/Scroll"

import Home from "./components/Home"
import Header2 from "./components/Header2"
import Profile from "./components/Profile"

import SignUp from "./components/SignUp"
import Login from "./components/Login"
import Logout from "./components/Logout"
import Forgot from "./components/Forgot"

import HMNavBar from "./components/HMNavBar"
import Posting from "./components/Posting"
import PostSuccess from "./components/PostSuccess"
import PostPage from "./components/PostPage"

import CNavBar from "./components/CNavBar"
import FoodListArray from "./components/FoodListArray"
import SearchBox from "./components/SearchBox"
import FoodList from "./components/FoodList"
import OrderHistory from "./components/OrderHistory"
import OrderSummary from "./components/OrderSummary"
import OrderPlaced from "./components/OrderPlaced"
import FoodDetails from "./components/FoodDetails"
import Cart from "./components/Cart"
import Review from "./components/Review"

const App = () => {
    return (
        <div>
            <Header />

            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/home" element={<Home />} />
                <Route path="/sign-up" element={<SignUp />} />
                <Route path="/sign-in" element={<Login />} />
                <Route path="/Logout" element={<Logout />} />
                <Route path="/Forgot" element={<Forgot />} />
                <Route path="/Profile" element={<Profile />} />
                <Route path="/Posting" element={<Posting />} />
                <Route path="/PostSuccess" element={<PostSuccess />} />
                <Route path="/PostPage" element={<PostPage />} />
                <Route path="/Foodlist" element={<FoodList />} />
                <Route path="/Search" element={<FoodList />} />
                <Route path="/FoodDetails" element={<FoodDetails />} />
                <Route path="/OrderHistory" element={<OrderHistory />} />
                <Route path="/Cart" element={<Cart />} />
                <Route path="/OrderPlaced" element={<OrderPlaced />} />
            </Routes>

            <Footer />
            <Scroll />
        </div>
    )
}

export default App
