import { Link } from "react-router-dom"
import HMNavBar from "./HMNavBar"

//Post success
const PostSuccess = () => {
    return (
        <div>
            <HMNavBar />
            <div className="auth-wrapper">
                <div className="auth-inner">
                    <br />
                    <h3>Posted Successfully</h3>
                    <div className="d-grid">
                            <Link
                                className="nav-link"
                                to={"/PostPage"}
                                class="main-btn btn-hover wow fadeInUp"
                                data-wow-delay=".6s"
                            >
                                Take Me to My Post
                            </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PostSuccess
