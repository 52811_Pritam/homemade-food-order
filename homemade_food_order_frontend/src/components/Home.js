import "../App.css"
import { Link } from "react-router-dom"
import Services from "./Services"
import About from "./About"
import How from "./How"
import Received from "./Received"
import Header2 from "./Header2"

// Home page
const Home = () => {
    return (
        <div>
            <Header2 />
            <section id="home" class="hero-section">
                <div class="hero-shape">
                    <img
                        src="assets/img/hero/hero-shape.svg"
                        alt=""
                        class="shape"
                    />
                </div>
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6">
                            <div class="hero-content">
                                <h1 class="wow fadeInUp" data-wow-delay=".2s">
                                    Delicious <br/>Home-made Food{" "}<br/>
                                    <span>At Your Doorstep</span>{" "}
                                </h1>
                                <p class="wow fadeInUp" data-wow-delay=".4s">
                                    Register with us and have the yummiest
                                    homemade food at your preferred time and venue.
                                </p>
                                <div className="d-grid">
                                    {/*<li class="nav-item">*/}
                                        <Link
                                            className="nav-link"
                                            to={"/sign-in"}
                                            class="main-btn btn-hover wow fadeInUp"
                                            data-wow-delay=".6s"
                                        >
                                            Home-Maker
                                        </Link>
                                        &nbsp;
                                        <Link
                                            className="nav-link"
                                            to={"/sign-in"}
                                            class="main-btn btn-hover wow fadeInUp"
                                            data-wow-delay=".6s"
                                        >
                                            Consumer
                                        </Link>
                                    {/*</li>*/}
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div
                                class="hero-img wow fadeInUp"
                                data-wow-delay=".5s"
                            >
                                <img
                                    src="assets/img/hero/hero-img.png"
                                    alt=""
                                />
                                {/*https://www.freepik.com/ & https://www.online-image-editor.com/ */}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <Services />
            <About />
            <How />
            <Received />
        </div>
    )
}

export default Home
