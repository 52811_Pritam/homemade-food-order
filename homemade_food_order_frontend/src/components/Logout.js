import React, { useEffect } from "react"
import { useNavigate } from "react-router"

const Logout = () => {
    
    let navigate = useNavigate()
    useEffect(() => {
        sessionStorage.removeItem("email")
        navigate("/")
    }, [])
}

export default Logout
