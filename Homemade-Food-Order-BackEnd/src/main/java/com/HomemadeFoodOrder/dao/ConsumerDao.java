package com.HomemadeFoodOrder.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.HomemadeFoodOrder.entity.ConsumerEntity;
import com.HomemadeFoodOrder.repository.ConsumerRepository;

@Service
public class ConsumerDao 
{
	@Autowired
	ConsumerRepository repo;
	
	public List<ConsumerEntity> getAll()
	{
		List<ConsumerEntity> list = repo.findAll();
		return list;
	}
	
	public String addConsumer(ConsumerEntity consumer)
	{
		repo.save(consumer);
		return "Consumer added & Saved";		
	}
	
	public void delete(int id)
	{
		repo.deleteById(id);
	}
	
	public ConsumerEntity changeHomePwd(@PathVariable String email, @PathVariable String newpwd)
	{
		ConsumerEntity cpwd = repo.findByEmail(email);
		if(cpwd != null)
		{
			cpwd.setPassword(newpwd);
			repo.save(cpwd);
			return cpwd;
		}
		return null;		
	}
	
	public ConsumerEntity getCheck(String email, String pwd)
	{
		ConsumerEntity con = repo.findByEmail(email);
		if(con != null)
		{
			if(pwd.equals(con.getPassword()))
			    return con;
			else 
				return null;
		}
		else
			return null;		
	}
}
