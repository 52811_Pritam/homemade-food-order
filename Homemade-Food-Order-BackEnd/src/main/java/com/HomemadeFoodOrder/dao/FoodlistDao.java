package com.HomemadeFoodOrder.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.HomemadeFoodOrder.entity.FoodlistEntity;
import com.HomemadeFoodOrder.repository.FoodlistRepository;

@Service
public class FoodlistDao 
{
	@Autowired
	FoodlistRepository repo;
	
	public List<FoodlistEntity> getAll()
	{
		List<FoodlistEntity> list = repo.findAll();
		return list;
	}
	
	public String addFood(FoodlistEntity food)
	{
		repo.save(food);
		return "Food added & Saved";		
	}
	
	public String delete(int id)
	{
		repo.deleteById(id);
		return "Food entry deleted";
	}
	
	public List<FoodlistEntity> getPosts(int id)
	{
		List<FoodlistEntity> post = repo.findByHomemakerId(id);
		System.out.println(post);
		return post;		
	}
}
