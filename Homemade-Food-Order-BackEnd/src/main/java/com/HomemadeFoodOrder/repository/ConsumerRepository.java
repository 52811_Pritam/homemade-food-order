package com.HomemadeFoodOrder.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.HomemadeFoodOrder.entity.ConsumerEntity;

public interface ConsumerRepository extends JpaRepository<ConsumerEntity, Integer>
{
	public ConsumerEntity findById(int i);
	public ConsumerEntity findByEmail(String n);
	public ConsumerEntity findByUsername(String n);
}
