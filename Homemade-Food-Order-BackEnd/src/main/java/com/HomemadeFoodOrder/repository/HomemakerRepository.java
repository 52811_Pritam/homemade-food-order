package com.HomemadeFoodOrder.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.HomemadeFoodOrder.entity.HomemakerEntity;

public interface HomemakerRepository extends JpaRepository<HomemakerEntity, Integer>
{
	public HomemakerEntity findById(int i);
	public HomemakerEntity findByEmail(String n);
}
