package com.HomemadeFoodOrder.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.HomemadeFoodOrder.dao.ConsumerDao;
import com.HomemadeFoodOrder.dao.HomemakerDao;
import com.HomemadeFoodOrder.entity.ConsumerEntity;
import com.HomemadeFoodOrder.entity.HomemakerEntity;
import com.HomemadeFoodOrder.repository.ConsumerRepository;
import com.HomemadeFoodOrder.repository.HomemakerRepository;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/profile")
public class SignupController 
{
	@Autowired
	HomemakerDao hdao;
	
	@Autowired
	HomemakerRepository hrepo;
	
	@Autowired
	ConsumerDao cdao;
	
	@Autowired
	ConsumerRepository crepo;
	
	@PostMapping("/addhomemaker")
	public String addHomemaker(@RequestBody HomemakerEntity h)
	{
		System.out.println(h.getFirstname());
		String str = hdao.addHomemaker(h);
		return "record inserted" + str;		
	}
	
	@GetMapping("/gethomemakers")
	public List<HomemakerEntity> getHomemaker()
	{
		List<HomemakerEntity> homemaker = hdao.getAll();
		return homemaker;
	}
	
	@PostMapping("/addconsumer")
	public String addConsumer(@RequestBody ConsumerEntity c)
	{
		System.out.println(c.getFirstname());
		String str = cdao.addConsumer(c);
		return "record inserted" + str;		
	}
	
	@GetMapping("/getconsumers")
	public List<ConsumerEntity> getConsumer()
	{
		List<ConsumerEntity> consumer = cdao.getAll();
		return consumer;
	}
	
	@PostMapping("/getprofile/{email}")
	public Object getProfile(@PathVariable String email)
	{
		HomemakerEntity hprof = hrepo.findByEmail(email);
		if(hprof !=null)
		{ 
			return hprof;
		}
		
		ConsumerEntity cprof = crepo.findByEmail(email);
		if(cprof !=null)
		{ 
			return cprof;
		}
		
		return null;		
	}
}
